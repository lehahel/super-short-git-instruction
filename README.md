# Как залить код на Github (под Windows)

### 1. Скачиваем установщик с [сайта](https://git-scm.com/downloads), убеждаетесь, что выставили настройку как на картинке! Остальные настройки не трогаем - оставляем как стоит
![](git-part2-4.jpg)

### 2. Теперь при нажатии правой кнопки в любой папке, у вас должны появиться пункты меню, как на картинке
![](git-bash.jpg)

### 3. Регистрируемся на [github](https://github.com)

### 4. Создаем новый репозиторий (при создании выбираем публичный репозиторий)
![](new-repo.png)

### 5. Заходим в репозиторий на github. Копируем ссылку
![](link.png)

### 6. Создаем папку. Кликаем в ней правой кнопкой мыши. Нажимаем "Git Bash Here". Должна открыться такая консоль
![](console.png)

### 7. Настраиваем гит и клонируем репозиторий, пишем в консоли:
`git config --global user.name Ваше имя`

`git config --global user.email Ваш email`

`git clone Ссылка, которую вы скопировали`

### 8. Поздравляю, вы скопировали репозиторий! Добавим в папку, которая появилась, ваши файлы с кодом

### 9. Когда весь код уже в папке, открываем Git Bash уже в папке репозитория, и пишем:
`git add .`

`git commit -m 'Любой текст'`

`git push origin HEAD`

### 10. После git push нужно будет ввести пароль (если попросят). Обратите внимание, что при вводе пароля, никаких символов/точек отображаться не будет, пароль надо будет вводить вслепую

![](push.png)

### 11. После этого, в вашем репозитории на github должен появиться ваш код. Проверьте!
![](code.png)

### 12. Если возникли проблемы или что-то не работает, пишите:
- <b>telegram:</b> @lexuxel
- <b>vk:</b> [@lehahel](https://vk.com/lehahel)

### 13. Полезные ссылки:
- [habr.com/ru/post/541258/](https://habr.com/ru/post/541258/) (Чтобы иметь общее представление о гите)
- [habr.com/ru/post/472600/](https://habr.com/ru/post/472600/) (Статья с основами гита)
- [learngitbranching.js.org](https://learngitbranching.js.org) (Тренажер по работе с гитом)
